import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ListeItemsComponent } from './containers/liste-items/liste-items.component';
import { ItemmComponent } from './components/itemm/itemm.component';
import { SharedModule } from '../shared/shared.module';
import { AddComponent } from './containers/add/add/add.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule, SharedModule, FormsModule

  ],
  declarations: [
    ListeItemsComponent,
    ItemmComponent,
    AddComponent
      ],
  exports: [ListeItemsComponent, AddComponent ]
})
export class ItemsModule { }
