import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NavComponent } from './components/nav/nav.component';
import { StateDirective } from './directive/state/state.directive';
import { FormComponent } from './components/form/form/form.component';

@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [NavComponent, StateDirective, FormComponent],
  exports: [NavComponent, StateDirective, FormComponent]
})
export class SharedModule { }
