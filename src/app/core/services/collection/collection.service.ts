import { Injectable } from '@angular/core';
import { COLLECTION } from '../collection';
import { Item } from '../../../shared/interface/item';


@Injectable()
export class CollectionService {
  private _collection: Item[];

  constructor() {
    this.collection = COLLECTION;

  }

  set collection(collection: Item[]) {

    this._collection = collection;

  }
  get collection(): Item[] {

    return this._collection;

  }



}
