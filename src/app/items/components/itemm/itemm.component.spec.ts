import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemmComponent } from './itemm.component';

describe('ItemmComponent', () => {
  let component: ItemmComponent;
  let fixture: ComponentFixture<ItemmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
