import { Component, OnInit } from '@angular/core';
import { Item} from '../../../shared/interface/item';
import { CollectionService } from '../../../core/services/collection/collection.service';


@Component({
  selector: 'app-liste-items',
  templateUrl: './liste-items.component.html',
  styleUrls: ['./liste-items.component.css']
})
export class ListeItemsComponent implements OnInit {
  collection: Item[];
  constructor(private collectionService: CollectionService) {}

  ngOnInit() {
    this.collection = this.collectionService.collection;
  }

}
