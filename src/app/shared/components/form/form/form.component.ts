import { Component, OnInit } from '@angular/core';
import { State } from '../../../enums/state.enum';
import { Item } from '../../../interface/item';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
state = State;
stateLibelles = Object.values(State);
newItem: Item;
  constructor() { }

  ngOnInit() {
    this.reset();
     }
  process(): void {
console.log(this.newItem);
this.reset();

  }

  reset() {

    this.newItem = {
      id: '',
      name: '',
      reference: '',
      state: State.ALIVER


    };

  }

}
