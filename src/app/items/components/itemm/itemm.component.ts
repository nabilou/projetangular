import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../shared/interface/item';
import { State } from '../../../shared/enums/state.enum';


@Component({
  selector: 'app-itemm',
  templateUrl: './itemm.component.html',
  styleUrls: ['./itemm.component.css']
})
export class ItemmComponent implements OnInit {
  @Input() item: Item;
  state = State;
    constructor() { }

  ngOnInit() {
  }
changeState(etat: State): void {

  this.item.state = etat;
}
}
