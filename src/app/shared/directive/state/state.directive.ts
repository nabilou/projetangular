import { Directive, Input } from '@angular/core';
import { OnInit } from '@angular/core';
import { element } from 'protractor';
import { HostBinding } from '@angular/core';
import { OnChanges } from '@angular/core';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnChanges {
 s1: string;
@Input() appState: string;
@HostBinding('class') elementClass: string;
  constructor() {


   }
   ngOnChanges() {
    this.elementClass = this.formatCssClass(this.appState);
    console.log(this.elementClass);

  }
  private formatCssClass(state: string): string {
    this.s1 = 'state-';
    return this.s1.concat(state.replace(' ','').toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') )
  }

}
